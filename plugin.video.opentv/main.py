import importlib, json, os, routing, xbmcaddon, xbmcgui, xbmcplugin, xbmcvfs

plugin = routing.Plugin()
addon = xbmcaddon.Addon()

@plugin.route("/")
def root():
    for network_id in get_networks():
        network = importlib.import_module(network_id)
        item = xbmcgui.ListItem(network.NAME)
        item.setArt({ "thumb": network.LOGO })
        xbmcplugin.addDirectoryItem(plugin.handle, plugin.url_for(index, network_id), item, True)
    xbmcplugin.endOfDirectory(plugin.handle)

@plugin.route("/index/<network_id>")
def index(network_id):
    channels = list_channels(network_id)
    xbmcplugin.addDirectoryItems(plugin.handle, channels, len(channels))
    xbmcplugin.endOfDirectory(plugin.handle)

@plugin.route("/play/<network_id>/<channel_id>")
def play(network_id, channel_id):
    network = importlib.import_module(network_id)
    item = network.play(channel_id)
    if item == None:
        return False
    listitem = xbmcgui.ListItem(label=item.get("label"), path=item.get("path"))
    if item.get("mimetype") != None:
        listitem.setMimeType(item.get("mimetype"))
        listitem.setContentLookup(False)
    if item.get("properties") != None:
        for name, value in item.get("properties").items():
            listitem.setProperty(name, value)
    xbmcplugin.setResolvedUrl(plugin.handle, True, listitem=listitem)

@plugin.route("/channels")
def channels():
    from lib.iptvmanager import IPTVManager
    port = int(plugin.args.get('port')[0])
    IPTVManager(port).send_channels()

@plugin.route("/epg")
def epg():
    from lib.iptvmanager import IPTVManager
    port = int(plugin.args.get('port')[0])
    IPTVManager(port).send_epg()

@plugin.route("/select_channels")
def select_channels():
    integration = get_integration()
    ids = dict() 
    options = []
    preselect = []
    index = 0
    for network_file in get_networks():
        network = importlib.import_module(network_file)
        for id, channel in list(network.channels().items()):
            name = network.NAME + " - " + channel.get("name")
            ids[index] = id
            options.append(name)
            if id in integration:
                preselect.append(index)
            index += 1
    dialog = xbmcgui.Dialog()
    selected_channels = dialog.multiselect(addon.getLocalizedString(30006), options, preselect=preselect)
    integration = []
    for index in selected_channels:
        integration.append(ids[index])
    save_integration(integration)

def get_networks():
    dir = os.path.join(os.path.dirname(__file__), "networks")
    for file in os.listdir(dir):
        if not file.endswith(".py") or file == "__init__.py":
            continue
        file = file.replace(".py", "")
        yield "networks." + file

def get_channels():
    integration = get_integration()
    for network_id in get_networks():
        network = importlib.import_module(network_id)
        channels = network.channels()
        for id, channel in list(channels.items()):
            if not id in integration:
                continue
            channel = dict(
                id = id,
                name = channel.get("name"),
                logo = channel.get("logo"),
                network = network.NAME,
                stream = plugin.url_for(play, network_id, id),
            )
            yield channel

def list_channels(network_id):
    integration = get_integration()
    network = importlib.import_module(network_id)
    channels = []
    for id, channel in list(network.channels().items()):
        item = xbmcgui.ListItem(channel.get("name"))
        item.setArt({ "thumb": channel.get("logo") })
        item.setProperty("IsPlayable", "true")
        url = plugin.url_for(play, network_id, id)
        channels.append((url, item, False))
    return channels

def get_epg():
    integration = get_integration()
    epgs = dict()
    for network in get_networks():
        network = importlib.import_module(network)
        ids = []
        for id, info in list(network.channels().items()):
            if not id in integration:
                continue
            ids.append(id)
        for id, epg in list(network.epg(ids).items()):
            epgs[id] = epg
    return epgs

def get_integration():
    try:
        with open(get_integration_path()) as f:
            settings = json.load(f)
    except Exception:
        settings = []
    return settings

def save_integration(data):
    with open(get_integration_path(), 'w') as f:
        json.dump(data, f, indent=4)

def get_integration_path():
    path = addon.getAddonInfo("profile")
    if path[:10] == "special://":
        path = xbmcvfs.translatePath(path)
    path = path.decode("utf8") if isinstance(path, bytes) else path
    if not os.path.exists(path):
        os.mkdir(path)
    return os.path.join(path, "integration.json")

if __name__ == '__main__':
    plugin.run()