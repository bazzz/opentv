import datetime, json, requests

# direct_url    = http://vs-cmaf-push-uk-live.akamaized.net/x=4/i=urn:bbc:pips:service:{0}/pc_hd_abr_v2_http.mpd
# direct_url2   = http://vs-cmaf-pushb-uk-live.akamaized.net/x=4/i=urn:bbc:pips:service:{0}/pc_hd_abr_v2_http.mpd
media_url = "https://open.live.bbc.co.uk/mediaselector/6/select/version/2.0/mediaset/pc/vpid/"
image_url = "https://i.ibb.co/"

NAME = "BBC"
LOGO = "https://www.bitcni.org.uk/wp-content/uploads/2018/09/BBC-logo.png"
CHANNELS = {
    "bbc_one_london":   { "name": "BBC one",    "logo": image_url + "ZLD0tZ8/BBCLowercase-10.png" },
    "bbc_two_hd":       { "name": "BBC two",    "logo": image_url + "zRpvqVY/BBCLowercase-11.png" },
    "bbc_three_hd":     { "name": "BBC three",  "logo": image_url + "LNqXQ9Z/BBCLowercase-12.png" },
    "bbc_four_hd":      { "name": "BBC four",   "logo": image_url + "L5vGh45/BBCLowercase-13.png" },
    "cbbc_hd":          { "name": "C",          "logo": image_url + "4SPp7X4/BBCLowercase-15.png" },
    "cbeebies_hd":      { "name": "CBeeBies",   "logo": image_url + "G5BZhNC/BBCLowercase-14.png" },   
}

def channels():
    return CHANNELS

def play(id):
    response = requests.get(media_url + id)
    data = response.json()
    connections = []
    for media in data["media"]:
        for connection in media["connection"]:
            connections.append(connection)
    connections = filter(lambda c: c["protocol"] == "http" and c["href"].endswith(".mpd") and c["transferFormat"] == "dash", connections)
    path = list(connections)[0]["href"]
    item = dict(
        label = CHANNELS[id].get("name"),
        path = path,
        mimetype = "application/xml+dash",
        properties = {
            "inputstream": "inputstream.adaptive",
            "inputstream.adaptive.manifest_type": "mpd",
            "inputstream.adaptive.live_delay": "16"
        }
    )
    return item

def epg(ids):
    epg = dict()
    now = datetime.datetime.now()
    m = str(now.month)
    if len(m) == 1:
        m = "0" + m
    d = str(now.day)
    if len(d) == 1:
        d = "0" + d
    date = str(now.year) + m + d
    for id in ids:
        epg[id] = []
        tag = id.replace("_london", "").replace("_hd", "").replace("_", "")
        url = "https://www.bbc.co.uk/iplayer/guide/" + tag + "/" + date
        response = requests.get(url)
        text = response.text
        key = "daysFromBBCToday\":0,\"items\":"
        pos = text.index(key)
        if pos < 0:
            continue
        pos += len(key)
        text = text[pos:]
        key = "},\"translations\":"
        pos = text.index(key)
        if pos < 0:
            continue
        text = text[:pos]
        data = json.loads(text)
        for item in data:
            epg[id].append(dict(
                start = item["meta"]["scheduledStart"],
                stop = item["meta"]["scheduledEnd"],
                title = item["props"]["title"],
                image = str(item["props"]["imageTemplate"]).replace("{recipe}", "462x260"),
                description = item["props"]["synopsis"],
            ))
    return epg