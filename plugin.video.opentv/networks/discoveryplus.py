import datetime, requests, os, uuid
from urllib.parse import urlencode

image_url = "https://eu1-prod-images.disco-api.com/"

NAME = "Discovery+"
LOGO = "https://www.discoveryplus.com/favicon.png"
CHANNELS = {
    "311": { "name": "Nove",         "logo": image_url + "2024/07/30/affa4007-f9f4-3495-8b66-45897522f436.png" },
    "310": { "name": "Real Time",    "logo": image_url + "2024/07/31/4067ad7c-c509-362f-bc04-7cc528ee8a14.png" },
    "323": { "name": "DMAX",         "logo": image_url + "2024/07/31/d7416f0e-3838-3426-9eca-bcd8794e54da.png" },
    "312": { "name": "Giallo",       "logo": image_url + "2023/11/14/02d21f46-cab0-33dd-8770-9a65a91b12df.png" },
    "866": { "name": "Warner TV",    "logo": image_url + "2024/07/29/e5b91881-1346-336a-be2d-cb96bb28b172.png" },
    "313": { "name": "K2",           "logo": image_url + "2024/06/27/d3ebd2a3-6596-3640-8556-9ac8b146053f.png" },
    "314": { "name": "Frisbee",      "logo": image_url + "2024/07/08/2086c81e-de99-357b-a4fa-64aa7cbc3ddf.png" },
    "315": { "name": "MotorTrend",   "logo": image_url + "2024/07/29/7238e757-005c-3ecc-9855-50f4236e5545.png" },
    "319": { "name": "Food Network", "logo": image_url + "2024/07/30/cc35baa0-c24d-30f9-bb85-a1377bec7d96.png" },
    "322": { "name": "HGTV",         "logo": image_url + "2024/07/29/e68dd78e-f583-3928-b44d-5a052adcb55a.png" }, 
}

def channels():
    return CHANNELS

def play(id):
    session = requests.Session()

    device_id = os.urandom(16).hex() #"bb8e7e8371df445c4434d209ea1aebaf"    
    url = "https://eu1-prod-direct.discoveryplus.com/token?deviceId=" + device_id + "&realm=dplay&shortlived=true"
    session.get(url) # Just to get the cookie.

    playback_id = str(uuid.uuid4()) # "bca3b0ee-db3f-4c84-ac5b-c7cc3e53534a"
    session_id = str(uuid.uuid4()) # "86f59fb8-d450-4528-90f4-91ba3f020aa0"
    url = "https://eu1-prod-direct.discoveryplus.com/playback/v3/channelPlaybackInfo"
    headers = {} 
    payload = {"channelId":id,"wisteriaProperties":{"advertiser":{"adId":"|||1722340942139a06a181ea3acc9453030577602dde4a1","firstPlay":0,"fwCcpa":"1---","fwDid":"","fwGdprConsent":"CQCkKjAQCkKjAAcABBENA_FwAAAAAAAAACiQAAAAAABjoAYAFQAQALzJQAQF5lIAYAFQAQALzAAA.YAAAAAAAAAAA","fwIsLat":1,"gpaln":"","interactiveCapabilities":["brightline"]},"appBundle":"undefined","device":{"browser":{"name":"edge","version":"127.0.0.0"},"id":"","language":"en","make":"","model":"","name":"edge","os":"Windows","osVersion":"NT 10.0","player":{"name":"Discovery Player Web","version":""},"type":"desktop"},"gdpr":1,"platform":"desktop","playbackId":playback_id,"product":"dplus_emea","sessionId":session_id,"siteId":"dplus_it","streamProvider":{"hlsVersion":6,"pingConfig":1,"suspendBeaconing":1,"version":"1.0.0"}},"deviceCapabilities":{"manifests":{"formats":{"dash":{}}},"segments":{"formats":{"fmp4":{}}},"codecs":{"audio":{"decoders":[{"codec":"aac","profiles":["lc","hev","hev2"]},{"codec":"ac3","profiles":[]}]},"video":{"decoders":[{"codec":"h264","profiles":["high","main","baseline"],"maxLevel":"5.2"},{"codec":"h265","profiles":["main10","main"],"maxLevel":"5.2"}],"hdrFormats":[]}},"contentProtection":{"contentDecryptionModules":[{"drmKeySystem":"widevine","maxSecurityLevel":"l1"}]},"devicePlatform":{"memory":{"allocatedMemory":0,"freeAvailableMemory":1.7976931348623157e+308},"network":{"capabilities":{"protocols":{"http":{"byteRangeRequests": True}}},"lastKnownStatus":{"networkTransportType":"unknown"}},"videoSink":{"capabilities":{"colorGamuts":["standard"],"hdrFormats":[]},"lastKnownStatus":{"height":2160,"width":3840}}}},"deviceInfo":{"adBlocker":False,"deviceId":"","drmTypes":{"widevine":True,"playready":False,"fairplay":False,"clearkey":False},"drmSupported":True,"hdrCapabilities":["SDR"],"hwDecodingCapabilities":["H264","H265"],"soundCapabilities":["STEREO","DOLBY_DIGITAL","DOLBY_DIGITAL_PLUS"],"screen":{"height":2160,"width":3840},"player":{"height":2160,"width":3840}}}
    response = session.post(url, headers=headers, json=payload)
    data = response.json()

    stream_info = data["data"]["attributes"]["streaming"][0]
    stream_url = stream_info["url"]
    drm_token = stream_info["protection"]["drmToken"]
    widevine_info = stream_info["protection"]["schemes"]["widevine"]
    # certificate_url = widevine_info["certificateUrl"]
    license_server_url = widevine_info["licenseUrl"]

    license_headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0",
        "Content-Type": "application/octet-stream",
        "Origin": "https://www.discoveryplus.com",
        "Priority": "u=1, i",
        "Referer" : "https://www.discoveryplus.com/",
        "Preauthorization": drm_token
    }
    license_config = {
        "license_server_url": license_server_url,
        "headers": urlencode(license_headers),
        "post_data": 'R{SSM}',
        "response_data": ''
    }
    item = dict(
        label = CHANNELS[id].get("name"),
        path = stream_url,
        contenttype = "application/xml+dash",
        properties = {
            "inputstream": "inputstream.adaptive",
            "inputstream.adaptive.license_type": "com.widevine.alpha",
            "inputstream.adaptive.license_key": '|'.join(license_config.values())
        }
    )
    return item

def epg(ids):
    epg = dict()
    return epg