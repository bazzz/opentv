import requests

NAME = "La C"
LOGO = "https://www.lacplay.it/assets/icon-v3/android-icon-192x192.png"

image_url = "https://cms-v1.lacplay.it/wp-content/uploads/"

session = requests.Session()

def channels():
    channels = {
        "lac-tv":       { "name": "La C TV",        "logo": image_url + "2022/01/LaCTv_Quadro.png"},
        "lac-news24":   { "name": "La C News24",    "logo": image_url + "2022/01/LaCNews24_Quadro.png"},
        "lac-onair":    { "name": "La C OnAir",     "logo": image_url + "2023/02/OnAir-icon.png"},
    }
    return channels

def play(id):
    channel = channels()[id]
    url = "https://www.lacplay.it/live-player/" + id + "/"
    response = session.get(url=url)
    text = response.text
    label = "<source src=\""
    start = text.index(label)
    text = text[start+len(label):]
    end = text.index("\" type=\"application/x-mpegURL")
    path = text[:end]
    item = dict(
        label = channel.get("name"),
        path = path
    )
    return item

def epg(ids):
    epg = dict()
    return epg