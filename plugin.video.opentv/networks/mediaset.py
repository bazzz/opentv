import datetime, requests, uuid
from urllib.parse import urlencode

image_url = "https://static2.mediasetplay.mediaset.it/mam/"

NAME = "Mediaset"
LOGO = "https://static3.mediasetplay.mediaset.it/static/images/mplay-logo-app192-v2.png"
CHANNELS = {
    "C5": { "name": "Canale 5",         "logo": image_url + "2021/03/30/164049140-2971546e-5c08-4907-a707-a9a4645fe34d@2.png" },
    "I1": { "name": "Italia 1",         "logo": image_url + "2021/03/30/165022887-1333663d-c4c2-4115-a7d0-13cc16eee6c8@2.png" },
    "R4": { "name": "Rete 4",           "logo": image_url + "2021/03/30/171446419-0fbcd8b0-aa93-43ad-9122-11eab918a692@2.png" },
    "LB": { "name": "20",               "logo": image_url + "2021/03/30/170242580-8ea87560-8f97-4134-9afb-b4b9e8230381@2.png" },
    "KA": { "name": "La 5",             "logo": image_url + "2021/03/30/170626282-d8fef159-5edc-49cb-a463-1489d44d6639@2.png" },
    "I2": { "name": "Italia 2",         "logo": image_url + "2021/03/30/170519488-02d50d65-cc5f-432e-acf5-83288c3a93ae@2.png" },
    "B6": { "name": "Cine34",           "logo": image_url + "2021/03/30/170905921-2bfee29f-6272-4f77-b631-7223950223da@2.png" },
    "KQ": { "name": "Mediaset Extra",   "logo": image_url + "2021/03/30/171152455-cd94ebc1-6ff6-468b-987b-87f6b3e41325@2.png" },
    "FU": { "name": "Focus",            "logo": image_url + "2021/03/30/171637742-621859dd-560c-4c8a-b800-93c1b5089b91@2.png" },
    "LT": { "name": "Top Crime",        "logo": image_url + "2021/03/30/171810575-cbce70dc-ddcc-43e3-85c7-d685d64e2cdd@2.png" },
    "KI": { "name": "Iris",             "logo": image_url + "2021/03/30/172013080-6fa2f27a-2a1f-43e9-86d0-ebe24e093ffe@2.png" },
    "KB": { "name": "Boing",            "logo": image_url + "2021/03/30/172218792-af5ce3a2-0253-415e-830d-4c3286328ff2@2.png" },
    "LA": { "name": "Cartoonito",       "logo": image_url + "2021/03/30/172548668-737d526b-743c-425e-82ab-0b206bf986c6@2.png" },
    "KF": { "name": "TGCom",            "logo": image_url + "2021/03/30/172718055-1ace4a32-2ded-45bd-b545-a02691db50e8@2.png" },
}

def channels():
    return CHANNELS

def play(id):
    session = requests.Session()

    response = session.get("https://mediasetinfinity.mediaset.it/diretta/canale5_cC5") # Any channel is fine to get the appName.
    text = response.text
    label = "meta name=\"app-name\" content=\""
    start = text.index(label)
    text = text[start+len(label):]
    end = text.index("\"")
    appName = text[:end]

    url = "https://api-ott-prod-fe.mediaset.net/PROD/play/idm/anonymous/login/v2.0"
    headers = { "Host": "api-ott-prod-fe.mediaset.net", "Referer": "https://mediasetinfinity.mediaset.it/", "Content-Type": "application/json", "Origin": "https://mediasetinfinity.mediaset.it" }
    payload = { "platform":"pc", "appName": appName, "client_id": str(uuid.uuid4()) }
    response = session.post(url, headers=headers, json=payload)
    data = response.json()
    token = data["response"]["beToken"]

    url = "https://static3.mediasetplay.mediaset.it/apigw/nownext/" + id + ".json"
    response = session.get(url)
    data = response.json()
    smilUrl = data.get("response").get("publicUrl")

    url = smilUrl + "?format=SMIL&auth=" + token + "&formats=MPEG-DASH&assetTypes=HR,browser,widevine,geoIT%7CgeoNo:HR,browser,geoIT%7CgeoNo:SD,browser,widevine,geoIT%7CgeoNo:SD,browser,geoIT%7CgeoNo:SS,browser,widevine,geoIT%7CgeoNo:SS,browser,geoIT%7CgeoNo&balance=true&auto=true&tracking=true&delivery=Streaming"
    response = session.get(url)
    text = response.text
    label = "<video src=\""
    start = text.index(label)
    text = text[start+len(label):]
    end = text.index("\"")
    path = text[:end]
    text = response.text
    label = "|pid="
    start = text.index(label)
    text = text[start+len(label):]
    end = text.index("|")
    releasePid = text[:end]

    license_server_url = "https://widevine.entitlement.theplatform.eu/wv/web/ModularDrm/getRawWidevineLicense?releasePid=" + releasePid + "&account=http%3A%2F%2Faccess.auth.theplatform.com%2Fdata%2FAccount%2F2702976343&schema=1.0&token=" + token
    license_headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36 Edg/127.0.0.0",
        "Content-Type": "application/octet-stream",
        "Origin": "https://mediasetinfinity.mediaset.it",
        "Priority": "u=1, i",
        "Referer": "https://mediasetinfinity.mediaset.it/",
        "Referrer-Policy": "strict-origin-when-cross-origin",
        "accept": "*/*",
    }
    license_config = {
        "license_server_url": license_server_url,
        "headers": urlencode(license_headers),
        "post_data": 'R{SSM}',
        "response_data": ''
    }
    item = dict(
        label = CHANNELS[id].get("name"),
        path = path,
        contenttype = "application/xml+dash",
        properties = {
            "inputstream": "inputstream.adaptive",
            "inputstream.adaptive.license_type": "com.widevine.alpha",
            "inputstream.adaptive.license_key": '|'.join(license_config.values())
        }
    )
    return item

def epg(ids):
    epg = dict()
    now = datetime.datetime.now()
    range_from = str(int(datetime.datetime(now.year, now.month, now.day, 0, 0, 0).timestamp())*1000)
    range_to = str(int(datetime.datetime(now.year, now.month, now.day +1, 0, 0, 0).timestamp())*1000)
    url = "https://api-ott-prod-fe.mediaset.net/PROD/play/feed/allListingFeedEpg/v2.0?byListingTime=" + range_from + "~" + range_to
    for id in ids:
        epg[id] = []
        response = requests.get(url + "&byCallSign=" + id)
        data = response.json()
        if not "response" in data:
            continue
        data = data["response"]["entries"][0]["listings"]
        for item in data:
            if not "startTime" in item:
                continue
            if not "endTime" in item:
                continue
            start_time = int(item["startTime"]) / 1000
            end_time = int(item["endTime"]) / 1000
            image = ""
            images = item["program"]["thumbnails"]
            if "image_horizontal_cover-704x396" in images:
                image = images["image_horizontal_cover-704x396"]["url"]
            elif "image_keyframe_poster-652x367" in images:
                image = images["image_keyframe_poster-652x367"]["url"]
            epg[id].append(dict(
                start = datetime.datetime.fromtimestamp(start_time).isoformat(),
                stop = datetime.datetime.fromtimestamp(end_time).isoformat(),
                title = item["mediasetlisting$epgTitle"],
                image = image,
                description = item["description"],
            ))
    return epg