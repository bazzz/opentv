

NAME = "Omroep Brabant"
LOGO = "https://media-exp1.licdn.com/dms/image/C510BAQEqcNmPBEQWdg/company-logo_200_200/0/1519856308065?e=2159024400&v=beta&t=u33VYxcC_pp8_QP2ioH504RqYdhJ8dRa0q7QUXXwgoc"
CHANNELS = {
    "omroep-brabant": { "name": "Omroep Brabant",    "logo": "https://media-exp1.licdn.com/dms/image/C510BAQEqcNmPBEQWdg/company-logo_200_200/0/1519856308065?e=2159024400&v=beta&t=u33VYxcC_pp8_QP2ioH504RqYdhJ8dRa0q7QUXXwgoc" }
}

def channels():
    return CHANNELS

def play(id):
    if id != "omroep-brabant": 
        return None
    item = dict(
        label = CHANNELS[id].get("name"),
        path = "https://d1ctz4dshc0ubt.cloudfront.net/live/omroepbrabant/tv/index.m3u8"
    )
    return item

def epg(ids):
    epg = dict()
    return epg