
NAME = "Nickelodeon"
LOGO = "https://centaur-wp.s3.eu-central-1.amazonaws.com/designweek/prod/content/uploads/2023/07/12124737/Banner-Image-12.jpg"

def channels():
    channels = {
        "nick-jr":   { "name": "Nick Jr.", "logo": "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/c82dd5b3-b0d8-44e8-b11d-a50e55b7b9de/dg23uz3-65d7c2c3-4c37-455c-8729-abc337d85370.png/v1/fill/w_1280,h_1202/nick_jr__logo__2023__by_carlosoof10_dg23uz3-fullview.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9MTIwMiIsInBhdGgiOiJcL2ZcL2M4MmRkNWIzLWIwZDgtNDRlOC1iMTFkLWE1MGU1NWI3YjlkZVwvZGcyM3V6My02NWQ3YzJjMy00YzM3LTQ1NWMtODcyOS1hYmMzMzdkODUzNzAucG5nIiwid2lkdGgiOiI8PTEyODAifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6aW1hZ2Uub3BlcmF0aW9ucyJdfQ.q72aai8EoO7m7dOZs3neIHhusPxv2-btk20Alc-AGgY"},
    }
    return channels

def play(id):
    channel = channels()[id]
    path = ""
    if id == "nick-jr": 
        path = "https://fl5.moveonjoy.com/NICK_JR/index.m3u8"
    item = dict(
        label = channel.get("name"),
        path = path
    )
    return item

def epg(ids):
    epg = dict()
    return epg