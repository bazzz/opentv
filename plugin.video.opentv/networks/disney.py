
NAME = "Disney"
LOGO = "https://logos-download.com/wp-content/uploads/2016/06/Walt_Disney_Pictures_logo.png"

def channels():
    channels = {
        "disney-channel":   { "name": "Disney Channel", "logo": "https://1000logos.net/wp-content/uploads/2020/09/Disney-Channel-Logo.png"},
        "disney-xd":        { "name": "Disney XD",     "logo": "https://logos-world.net/wp-content/uploads/2023/06/Disney-XD-Logo.png"},
    }
    return channels

def play(id):
    channel = channels()[id]
    path = ""
    if id == "disney-channel": 
        path = "https://fl5.moveonjoy.com/DISNEY_CHANNEL/index.m3u8"
    elif id == "disney-xd":
        path = "https://fl5.moveonjoy.com/DISNEY_XD/index.m3u8"
    item = dict(
        label = channel.get("name"),
        path = path
    )
    return item

def epg(ids):
    epg = dict()
    return epg