import re, requests
from urllib.parse import urlencode

reg_manifest = re.compile(r'\'(.*?)mpd')

NAME = "La7"
LOGO = "http://tvstreamingonline.org/images/LOGHIGRANDI/la7.jpg"
CHANNELS = { 
    "la7":  { "name": "La7",    "logo": "http://tvstreamingonline.org/images/LOGHIGRANDI/la7.jpg" },
    "la7d": { "name": "La7d",   "logo": "https://vignette.wikia.nocookie.net/logopedia/images/9/98/La7d.png/revision/latest" },
}

def channels():
    return CHANNELS

def play(id):
    live_url = "http://www.la7.it/dirette-tv" if id == "la7" else "https://www.la7.it/live-la7d"
    response = requests.get(live_url)
    text = response.text
    path = reg_manifest.findall(text)[0] + "mpd"
    pos = text.index("preTokenUrl = \"") + len("preTokenUrl = \"")
    text = text[pos:]
    auth_url = text[:text.index("\"")]
    response = requests.get(auth_url)
    token = response.json()["preAuthToken"]
    license_headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/109.0",
        "Content-Type": "application/octet-stream",
        "preauthorization": token,
    }
    license_config = {
        "license_server_url": "https://la7.prod.conax.cloud/widevine/license",
        "headers": urlencode(license_headers),
        "post_data": 'R{SSM}',
        "response_data": '',
    }
    item = dict(
        label = CHANNELS[id].get("name"),
        path = path,
        mimetype = "application/xml+dash",
        properties = {
            "inputstream": "inputstream.adaptive",
            "inputstream.adaptive.manifest_type": "mpd",
            "inputstream.adaptive.license_type": "com.widevine.alpha",
            "inputstream.adaptive.license_key": '|'.join(license_config.values())
        }
    )
    return item

def epg(ids):
    epg = dict()
    return epg