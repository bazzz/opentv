import requests

NAME = "Rakuten TV Italia"
LOGO = "https://logos-world.net/wp-content/uploads/2023/01/Rakuten-Logo.png"

session = requests.Session()
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:128.0) Gecko/20100101 Firefox/128.0",
    "Accept": "application/json, text/plain, */*",
    "Accept-Language": "en-US,en;q=0.5",
    "Content-Type": "application/json",
    "Referrer": "https://www.rakuten.tv/",
}

def channels():
    url = "https://gizmo.rakuten.tv/v3/live_channels?classification_id=36&device_identifier=web&device_stream_audio_quality=2.0&device_stream_hdr_type=NONE&device_stream_video_quality=FHD&locale=it&market_code=it&per_page=999"
    response = session.get(url=url, headers=headers)
    data = response.json()
    channel_list = list()
    for item in data["data"]:
        name = item["title"]
        if name.endswith(" - Rakuten TV"):
            name = name[:-len(" - Rakuten TV")]
        language = item["labels"]["languages"][0]["id"]
        if len(item["labels"]["languages"]) > 1:
            for lang in item["labels"]["languages"]:
                if lang["id"] == "ITA":
                    language = "ITA"
                    break
        channel = {
            "id": item["id"],
            "name": name,
            "number": item["channel_number"],
            "logo": item["images"]["artwork"],
            "language": language,
        }
        channel_list.append(channel)
    channel_list.sort(key=lambda x: x["number"])
    channels = dict()
    for channel in channel_list:
        channels[channel["id"]] = channel
    return channels

def play(id):
    channel = channels()[id]
    url = "https://gizmo.rakuten.tv/v3/avod/streamings?classification_id=36&device_identifier=web&device_stream_audio_quality=2.0&device_stream_hdr_type=NONE&device_stream_video_quality=FHD&disable_dash_legacy_packages=false&locale=it&market_code=it"
    payload = {
        "audio_language": channel.get("language"),
        "audio_quality": "2.0",
        "classification_id": "36",
        "content_id": id,
        "content_type": "live_channels",
        "device_make": "firefox",
        "device_model": "GENERIC",
        "device_serial": "not implemented",
        "device_stream_video_quality": "HD",
        "device_year": 1970,
        "gam_correlator": 1369997557139616,
        "gdpr_consent_opt_out": "1",
        "hdr_type": "NONE",
        "ifa_type": "ppid",
        "player_height": 1080,
        "player_width": 1920,
        "player": "web:HLS-NONE:NONE",
        "strict_video_quality": False,
        "subtitle_formats": ["vtt"],
        "subtitle_language": "MIS",
        "support_closed_captions": True,
        "video_type": "stream",
        "support_thumbnails": True,
        "app_bundle": "com.rakutentv.web",
        "app_name": "RakutenTV",
        "url": "rakuten.tv",
    }
    response = session.post(url=url, headers=headers, json=payload)
    data = response.json()
    url = data["data"]["stream_infos"][0]["url"]
    item = dict(
        label = channel.get("name"),
        path = url[:url.index("?")]
    )
    return item

def epg(ids):
    epg = dict()
    return epg