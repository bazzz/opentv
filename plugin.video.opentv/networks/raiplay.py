import datetime, requests

base_url = "https://www.raiplay.it/"
content_url = base_url + "dirette/"
epg_url = base_url + "palinsesto/app/"
image_url = "https://images2.imgbox.com/"
user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.56"

NAME = "Raiplay"
LOGO = base_url + "dl/components/img/logo-app.png"
CHANNELS = {
    "rai-1":         { "name": "Rai 1",          "logo": image_url + "da/97/yHXTwMxX_o.png" },
    "rai-2":         { "name": "Rai 2",          "logo": image_url + "91/31/xXDeyrY5_o.png" },
    "rai-3":         { "name": "Rai 3",          "logo": image_url + "54/cd/EIc4942w_o.png" },
    "rai-4":         { "name": "Rai 4",          "logo": image_url + "27/b4/PQadTynY_o.png" },
    "rai-5":         { "name": "Rai 5",          "logo": image_url + "f3/64/yCZq81pw_o.png" },
    "rai-movie":     { "name": "Rai Movie",      "logo": image_url + "13/19/eZzkaPd7_o.png" },
    "rai-premium":   { "name": "Rai Premium",    "logo": image_url + "92/37/0hGNqFZU_o.png" },
    "rai-storia":    { "name": "Rai Storia",     "logo": image_url + "c3/1f/X4ciCpVu_o.png" },
    "rai-yoyo":      { "name": "Rai Yoyo",       "logo": image_url + "50/d8/1kbHtg8X_o.png" },
    "rai-gulp":      { "name": "Rai Gulp",       "logo": image_url + "63/8d/ju9vE3r6_o.png" },
    "rai-news24":    { "name": "Rai News 24",    "logo": image_url + "93/90/YlKZxeqp_o.png" },
    "rai-sport":     { "name": "Rai Sport",      "logo": image_url + "39/9d/oumTE8Nm_o.png" },
    "rai-scuola":    { "name": "Rai Scuola",     "logo": image_url + "c6/a7/QiLCbTPO_o.png" },
}

def channels():
    return CHANNELS

def play(id):
    url = content_url + id.replace("-", "") + ".json"
    client = requests.Session()
    client.headers.update({"User-Agent" : user_agent})
    client.headers.update({"Host" : "www.raiplay.it"})
    response = client.get(url)
    url = response.json()["video"]["content_url"] + "&output=64"
    client.headers.update({"Host" : "mediapolis.rai.it", "Accept-Encoding" : "gzip, deflate, br", "Referer" : base_url, "Origin" : base_url })
    response = client.get(url)
    data = response.text
    start = data.index("<![CDATA[https://") + len("<![CDATA[")
    end = data.index("]]>")
    path = data[start:end]
    item = dict(
        label = CHANNELS[id].get("name"),
        path = path,
        mimetype = "application/x-mpegURL",
        properties = {
            "inputstream": "inputstream.adaptive",
            "inputstream.adaptive.manifest_type": "hls",
            "inputstream.adaptive.live_delay": "128" # Magic number based on my Raspberry Pi 3B experiments.
        }
    )
    return item

def epg(ids):
    epg = dict()
    now = datetime.datetime.now()
    now_text = now.strftime('%d') + "-" + now.strftime('%m') + "-" + now.strftime('%Y')
    for id in ids:
        epg[id] = []
        url = epg_url + id + "/" + now_text + ".json"  
        response = requests.get(url)
        data = response.json()
        events = data["events"]
        for item in events:
            dateparts = [int(x) for x in item["date"].split("/")]
            timeparts = [int(x) for x in item["hour"].split(":")]
            durationparts = [int(x) for x in item["duration"].split(":")]
            start = datetime.datetime(dateparts[2], dateparts[1], dateparts[0], timeparts[0], timeparts[1]).astimezone()
            end = start + datetime.timedelta(hours=durationparts[0], minutes=durationparts[1], seconds=durationparts[2])
            epg[id].append(dict(
                start = start.isoformat(),
                stop = end.isoformat(),
                title = item["name"],
                image = base_url + item["image"],
                description = item["description"],
            ))
    return epg