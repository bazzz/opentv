import json, socket

class IPTVManager:
    """Interface to IPTV Manager"""

    def __init__(self, port):
        """Initialize IPTV Manager object"""
        self.port = port

    def via_socket(func):
        """Send the output of the wrapped function to socket"""
        def send(self):
            """Decorator to send over a socket"""
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect(('127.0.0.1', self.port))
            try:
                sock.sendall(json.dumps(func()).encode())
            finally:
                sock.close()
        return send

    @via_socket
    def send_channels():
        """Return JSON-M3U formatted information to IPTV Manager"""
        from main import get_channels
        channels = []
        for channel in get_channels():
            channels.append(channel)
        return dict(version=1, streams=channels)

    @via_socket
    def send_epg():
        """Return JSONTV formatted information to IPTV Manager"""
        from main import get_epg
        return dict(version=1, epg=get_epg())
