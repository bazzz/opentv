# OpenTV

Project OpenTV is a Kodi add-on that adds free open internet tv channels to the Kodi TV integration.

## Installation

Your Kodi installation needs to have InputStream Adaptive installed.

## Disclaimer

Note that OpenTV does not bypass geo restrictions. It only allows you to watch streams that you can legally watch in your region.
